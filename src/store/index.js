import Vue from 'vue'
// import { set } from 'vue/types/umd'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
   
  
    coaches:[],
    requests:[]

  },
  mutations: {
    //setters

    register(state,coach){
      state.coaches.push(coach)
    },
    setCoaches(state, payload) {
      state.coaches.push(payload);
    },
    addRequest(state, payload){
      state.requests = payload;
    },
    setRequests(state, payload) {
      state.requests = payload;
    },
    
    async loadCoaches(state) {

      const response = await fetch(`https://find-the-coach-default-rtdb.firebaseio.com/coaches.json`);

      const responseData = await response.json();

      console.log("resp",response)
  
      if (!response.ok) {
        console.log("error",response)
        // const error = new Error(responseData.message || 'Failed to fetch!');
        // throw error;
      }
  
      const coaches = [];
  
      for (const key in responseData) {
        console.log("key",key)
        const coach = {
          id: key,
          firstName: responseData[key].firstName,
          lastName: responseData[key].lastName,
          description: responseData[key].description,
          hourlyRate: responseData[key].hourlyRate,
          areas: responseData[key].areas
        };
        coaches.push(coach);
      }
  
       state.coaches = coaches
    },


  },
  actions: {

    async registerCoach(context,data) {
      
      const userId = Date.now()
      const coachData = {
        firstName: data.first,
        lastName: data.last,
        description: data.desc,
        hourlyRate: data.rate,
        areas: data.areas
      };


    const response = await fetch(
        `https://find-the-coach-default-rtdb.firebaseio.com/coaches/${userId}.json`,
        {
          method: 'PUT',
          body: JSON.stringify(coachData)
        }
      );
    
      if (!response.ok) {
        console.log("Somthing went wrong")
      }

      context.commit('register', {
        ...coachData,
        id: userId
      });
    
    },
    async contactCoach(context, payload) {
      const newRequest = {
        userEmail: payload.email,
        message: payload.message
      };
      const response = await fetch(`https://find-the-coach-default-rtdb.firebaseio.com/requests/${payload.coachId}.json`, {
        method: 'POST',
        body: JSON.stringify(newRequest)
      });
  
      const responseData = await response.json();
  
      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to send request.');
        throw error;
      }
  
      newRequest.id = responseData.name;
      newRequest.coachId = payload.coachId;
  
      context.commit('addRequest', newRequest);
    },

    async fetchRequests(context) {

      const coachId = 1622785317974
      // console.log("coachId",context.rootGetters.coaches)
      const response = await fetch(`https://find-the-coach-default-rtdb.firebaseio.com/requests/${coachId}.json`);
      const responseData = await response.json();
      console.log("rsp",responseData)

      if (!response.ok) {
        const error = new Error(responseData.message || 'Failed to fetch requests.');
        throw error;
      }
  
      const requests = [];
  
      for (const key in responseData) {
        console.log(responseData[key].message)
        console.log(key)
        const request = {
          id: key,
          coachId: coachId,
          userEmail: responseData[key].userEmail,
          message: responseData[key].message
        };
        requests.push(request);
        console.log("rrr",request)
      }
  
      context.commit('setRequests', requests);
    }


    },
    

  
  getters: {
    coaches(state) {
      return state.coaches
    },
    hasCoaches(state){
      return state.coaches && state.coaches.length > 0;
    },
    requests(state){
      return state.requests
    },
    hasRequests(state){
      return state.requests && state.requests.length > 0;
    }
  }


})
