import Vue from 'vue'
import VueRouter from 'vue-router'

import CoachesList from '../components/coaches/CoachesList.vue';
import CoachDetails from  '../components/coaches/CoachDetails.vue';
import CoachRegistration from '../components/coaches/CoachRegistration.vue';

import ContactCoach from '../components/requests/ContactCoach.vue';
import RequestReceived from '../components/requests/RequestReceived.vue';
import NotFound from '../components/NotFound.vue';


Vue.use(VueRouter)

const routes = [
  {
    path:'/' ,
    redirect:'/coaches'
   }
   ,
   { 
    path: '/coaches',
    name:'coacheslist',
    component: CoachesList
  },
  { 
    path: '/coaches',
    component:CoachesList,
  },
    
  {
    path:'/coaches/:id',
    component:CoachDetails,
    name:'coaches',
    props:true,
    children:[
      {
        path:'contact',
        component:ContactCoach 
      }
    ]
  },

  {
    path:'/register',
    component:CoachRegistration,
    
  },
  {
    path:'/requests',
    component:RequestReceived,
    
  },
  {
   path:'/:notFound(.*)',
   component:NotFound
  
  }
 
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
